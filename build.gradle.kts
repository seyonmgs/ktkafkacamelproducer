import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.2.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    kotlin("jvm") version "1.3.50"
    kotlin("plugin.spring") version "1.3.50"
}

group = "net.gowri"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
}

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://packages.confluent.io/maven/") }
    jcenter()
}

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath("com.commercehub.gradle.plugin:gradle-avro-plugin:0.17.0")
    }
}

apply(plugin = "com.commercehub.gradle.plugin.avro")

extra["springCloudVersion"] = "Hoxton.RC1"
extra["camelVersion"] = "3.0.0-RC3"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
//    implementation("org.springframework.cloud:spring-cloud-starter-netflix-hystrix")

    compile("io.confluent:common-config:5.2.1")
    compile("io.confluent:kafka-avro-serializer:5.2.1")
    compile("io.confluent:kafka-schema-registry-client:5.2.1")
    compile("org.apache.kafka:kafka_2.11")
    compile("org.apache.avro:avro:1.9.1")
    compile("io.github.microutils:kotlin-logging:1.7.6")

    compile("ch.qos.logback.contrib:logback-json-classic:0.1.5")
    compile("ch.qos.logback.contrib:logback-jackson:0.1.5")


    compile("org.apache.camel:camel-spring-boot-starter:${property("camelVersion")}")
    compile("org.apache.camel:camel-avro-starter:${property("camelVersion")}")
    compile("org.apache.camel:camel-servlet-starter:${property("camelVersion")}")
    compile("org.apache.camel:camel-kafka:${property("camelVersion")}")
    compile("org.apache.camel:camel-support:${property("camelVersion")}")



    developmentOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.apache.camel:camel-test-spring:${property("camelVersion")}")
    testImplementation("org.apache.camel:camel-test:${property("camelVersion")}")
}

//dependencyManagement {
//    imports {
//        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
//    }
//}

sourceSets {
    main {
        java.srcDir("${buildDir}/generated/java")
    }
}


tasks.register<com.commercehub.gradle.plugin.avro.GenerateAvroJavaTask>("generateAvro") {
    source = fileTree("src/main/resources/avsc")
    setOutputDir(file("${buildDir}/generated/java"))
}

tasks.register<Exec>("kafkaUp") {
    group = "kafka"
    commandLine("confluent", "local", "start")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}

tasks.register<Exec>("kafkaDown") {
    group = "kafka"
    commandLine("confluent", "local", "stop")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}


tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
    source(tasks["generateAvro"].outputs)
}
