package net.gowri.kafkacamelproducer.processor

import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.kafka.clients.producer.RecordMetadata
import org.springframework.stereotype.Component

@Component
class ResponseValidator : Processor {
    override fun process(exchange: Exchange?) {
        @Suppress("UNCHECKED_CAST")
        val kafkaResponseList :ArrayList<RecordMetadata> =
            exchange?.getIn()?.getHeader("org.apache.kafka.clients.producer.RecordMetadata", ArrayList::class) as ArrayList<RecordMetadata>
        val recordMetadata = kafkaResponseList.get(0)
        val responseText = if (recordMetadata.offset() > 0) "successful- on partition " +
                "${recordMetadata.partition()}" else "failure publishing to topic : ${recordMetadata.topic()}"
        exchange.getIn().setBody(responseText, String::class.java)

    }

}
