package net.gowri.kafkacamelproducer.processor

import io.confluent.gowri.basicavro.Payment
import mu.KotlinLogging
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.stereotype.Component

@Component
class KafkaMessageProcessor : Processor {
    private val log = KotlinLogging.logger {}

    override fun process(exchange: Exchange?) {
        log.info { "creating message" }
        exchange?.getIn()?.setBody(Payment("camel", 300.95), Payment::class.java)
        log.info{"created message"}
    }

}
