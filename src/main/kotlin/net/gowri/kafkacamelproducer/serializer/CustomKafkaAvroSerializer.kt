package net.gowri.kafkacamelproducer.serializer

import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import io.confluent.kafka.serializers.AbstractKafkaAvroSerializer
import io.confluent.kafka.serializers.AvroSchemaUtils
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig
import mu.KotlinLogging
import net.gowri.kafkacamelproducer.config.SpringContext
import org.springframework.stereotype.Component

@Component
class CustomKafkaAvroSerializer : AbstractKafkaAvroSerializer(), org.apache.kafka.common.serialization.Serializer<Any>

{
    private val log = KotlinLogging.logger {}
    var isKey = false
    var useSpecificAvroReader = true

    override fun serialize(topic: String?, data: Any?): ByteArray {
        log.info { "custom serialiser is triggered" }
        return serializeImpl(getSubjectName(topic, false, data, AvroSchemaUtils.getSchema(data)), data)
    }

    override fun configure(config: KafkaAvroSerializerConfig?) {
        this.schemaRegistry = SpringContext.appContext.getBean("cachedSchemaRegistryClient") as SchemaRegistryClient?
        this.useSpecificAvroReader=true
        this.isKey = false;
    }

    override fun close() {
    }

    override fun configure(configs: MutableMap<String, *>?, arg: Boolean) = configure(null)
}

