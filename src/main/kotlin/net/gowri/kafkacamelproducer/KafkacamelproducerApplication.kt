package net.gowri.kafkacamelproducer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KafkacamelproducerApplication

fun main(args: Array<String>) {
    runApplication<KafkacamelproducerApplication>(*args)
}
