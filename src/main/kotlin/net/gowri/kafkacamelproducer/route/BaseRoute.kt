package net.gowri.kafkacamelproducer.route

import net.gowri.kafkacamelproducer.processor.ResponseValidator
import net.gowri.kafkacamelproducer.processor.KafkaMessageProcessor
import org.apache.camel.builder.RouteBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Component

@Component
class BaseRoute : RouteBuilder(){
    @Autowired
    lateinit var messageProcessor : KafkaMessageProcessor

    @Autowired
    lateinit var responseValidator : ResponseValidator

    override fun configure() {
        val DIRECTMESSAGE = "direct:message"
        rest("/")
            .consumes(MediaType.APPLICATION_JSON_VALUE)
            .post("message")
            .to(DIRECTMESSAGE)

//        transform(simple("A simple hello from the other side"))
        from(DIRECTMESSAGE)
            .process(messageProcessor)
            .log("Sending message to kafka from camel")
            .to("kafka:{{producer.topic}}?brokers={{kafka.bootstrap.url}}" +
                    "&keySerializerClass=org.apache.kafka.common.serialization.StringSerializer" +
                    "&serializerClass=net.gowri.kafkacamelproducer.serializer.CustomKafkaAvroSerializer")
            .process(responseValidator)
            .transform(simple("\${body}"))
//            .transform(simple("A simple hello from the other side"))

    }

}