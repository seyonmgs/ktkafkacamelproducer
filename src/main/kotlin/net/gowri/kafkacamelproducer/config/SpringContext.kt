package net.gowri.kafkacamelproducer.config

import org.springframework.beans.BeansException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

@Component
class SpringContext : ApplicationContextAware  {

    @Throws(BeansException::class)
    override fun setApplicationContext(applicationContext: ApplicationContext) {
        appContext = applicationContext
    }

    companion object {
        @Autowired
        lateinit var appContext: ApplicationContext
            internal set
    }
}