package net.gowri.kafkacamelproducer.config

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SchemaRegistryClientConfiguration {
    @Value("\${schema.registry.url}")
    lateinit var schemaRegistryUrl: String
    @Bean
    fun cachedSchemaRegistryClient() : SchemaRegistryClient{
        return CachedSchemaRegistryClient(schemaRegistryUrl, Int.MAX_VALUE)
    }
}
